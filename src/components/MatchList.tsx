import {Button, FlatList, NativeScrollEvent, NativeSyntheticEvent} from 'react-native';
import React from 'react';
import MostRecentMatch, {MOST_RECENT_HEIGHT_LIST_ITEM} from './MostRecentMatch';
import DefaultMatch, {DEFAULT_HEIGHT_LIST_ITEM, DEFAULT_MARGIN_LIST_ITEM} from './DefaultMatch';

interface MatchListProps {
    matches: Match[],
    onBottomCallback: () => void,
    onTopCallback: () => void
}

let flatListRef: FlatList<Match>;
const mostRecentItemHeight = MOST_RECENT_HEIGHT_LIST_ITEM + DEFAULT_MARGIN_LIST_ITEM * 2;
const defaultHeight = DEFAULT_HEIGHT_LIST_ITEM + DEFAULT_MARGIN_LIST_ITEM * 2;

const getItemLayout = (data: Match[] | null | undefined, index: number) => {
    if (index > 0 && data && data[index].isMostRecent) {
        return {
            length: mostRecentItemHeight,
            offset: defaultHeight * (index - 1) + mostRecentItemHeight,
            index
        }
    }
    return {
        length: defaultHeight,
        offset: defaultHeight * index,
        index
    }
}

let previousListSize = 0;
let previousScrollPosition = 0;
let updating = false;

const MatchList: React.FC<MatchListProps> = ({matches, onBottomCallback, onTopCallback}) => {
    const scrollToMostRecentMatch = (animate: boolean) => {
        flatListRef.scrollToIndex({index: matches.findIndex(match => match.isMostRecent), animated: animate, viewPosition: 0.5});
    };

    const onContentSizeChanged = (width: number, height: number) => {
        if (updating) {
            flatListRef.scrollToOffset({offset: (height - previousListSize) + previousScrollPosition, animated: false});
            setTimeout(() => updating = false, 100);
        }
        previousListSize = height;
    };

    const onScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
        if (event.nativeEvent.contentOffset.y < 200 && !updating) {
            updating = true;
            onTopCallback();
        }
        previousScrollPosition = event.nativeEvent.contentOffset.y;
    }

    return (
        <>
            <Button title='Back to most recent' onPress={() => scrollToMostRecentMatch(true)}/>
            <FlatList
                getItemLayout={getItemLayout}
                keyExtractor={(match) => match.name}
                data={matches}
                showsVerticalScrollIndicator={false}
                renderItem={({item}) => item.isMostRecent ? <MostRecentMatch match={item}/> : <DefaultMatch match={item}/>}
                ref={(ref) => flatListRef = ref as FlatList<Match>}
                onLayout={() => scrollToMostRecentMatch(false)}
                onContentSizeChange={onContentSizeChanged}
                onEndReachedThreshold={0.8}
                onEndReached={onBottomCallback}
                onScroll={onScroll}
            />
        </>
    );
}

export default MatchList;
