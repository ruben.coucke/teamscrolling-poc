import {StyleSheet, Text, View} from 'react-native';
import React, {memo} from 'react';
import {DEFAULT_MARGIN_LIST_ITEM} from './DefaultMatch';

export const MOST_RECENT_HEIGHT_LIST_ITEM = 120;

interface MostRecentMatchProps {
    match: Match
}

const MostRecentMatch: React.FC<MostRecentMatchProps> = ({match}) => {
    return (
        <View style={styles.container}>
            <Text>Upcoming match</Text>
            <Text>{match.name}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: MOST_RECENT_HEIGHT_LIST_ITEM,
        margin: DEFAULT_MARGIN_LIST_ITEM
    }
});

export default memo(MostRecentMatch);
