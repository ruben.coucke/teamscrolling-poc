import {StyleSheet, Text, View} from 'react-native';
import React, {memo} from 'react';

export const DEFAULT_HEIGHT_LIST_ITEM = 100;
export const DEFAULT_MARGIN_LIST_ITEM = 5;

interface DefaultMatchProps {
    match: Match
}

const DefaultMatch: React.FC<DefaultMatchProps> = ({match}) => {
    return (
        <View style={styles.container}>
            <Text>{match.name}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: DEFAULT_HEIGHT_LIST_ITEM,
        margin: DEFAULT_MARGIN_LIST_ITEM,
    }
});

export default memo(DefaultMatch);
