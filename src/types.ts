interface Match {
    name: string;
    points: number;
    isMostRecent?: boolean;
}
