import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {randomAlphabetic, randomNumeric} from '../random';
import MatchList from '../components/MatchList';

const randomMatch = (index: number = 0): Match => {
    return {
        name: `Index: ${index}. ${randomAlphabetic()}`,
        points: randomNumeric()
    }
}

const createRandomMatches = (): Match[] => {
    const newMatches: Match[] = [];
    for (let i = 0; i < 20; i++) {
        newMatches.push(randomMatch(i));
    }
    newMatches[Math.floor(5 + Math.random() * 10)].isMostRecent = true;
    return newMatches;
};

const ListScreen = () => {
    const [matches, setMatches] = useState<Match[]>([]);

    useEffect(() => {
        setMatches(createRandomMatches())
    }, []);

    const addEarlierMatches = () => {
        console.log("Added 20 earlier items");
        const newMatches: Match[] = [];
        for (let i = 0; i < 20; i++) {
            newMatches.push(randomMatch(i));
        }
        return setMatches([...newMatches, ...matches]);
    }

    const addLaterMatches = () => {
        console.log("Added 20 later items");
        const newMatches: Match[] = [];
        for (let i = 0; i < 20; i++) {
            newMatches.push(randomMatch(i + matches.length));
        }
        return setMatches([...matches, ...newMatches]);
    }

    return (
        <SafeAreaView style={styles.container}>
            <Text style={styles.center}>Here are some other views.</Text>
            <View style={styles.list}>
                {matches.length ? <MatchList onBottomCallback={addLaterMatches} onTopCallback={addEarlierMatches} matches={matches}/> : null}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    list: {
        height: 500,
        backgroundColor: '#EFEFEF',
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        borderTopWidth: 1,
        borderTopColor: 'black',
        marginBottom: 50
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    center: {
        alignSelf: 'center',
        flex: 1,
        fontSize: 40,
        textAlign: 'center'
    }
});

export default ListScreen;
